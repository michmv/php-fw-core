# Core for my framework :)

Core of php framework for build web application.

## Install example

In public directory create the file:

```php
<?php // index.php
use MMV\FW\Core\ApplicationInterface;
use MMV\FW\Core\ApplicationTrait;
use MMV\FW\Core\RoutesInterface;
use MMV\FW\Core\Execute;

require_once(__DIR__.'/../vendor/autoload.php');

class Web implements ApplicationInterface {
    use ApplicationTrait;
    public static function getRoutes(ApplicationInterface $app): RoutesInterface {
        return (new class() implements RoutesInterface {
            public function getExecute(): Execute {
                return new Execute( ... );
            }
        });
    }
}
$config = ['debug' => false];
echo Web::main($config);
```

Open `index.php` in browser.

## Documentation

See class `MMV\FW\Core\ApplicationTrait` for get more information how application live.

### Execute class

Can use two types of path to launch a custom function: controller or callback function.

#### Callback function

```php
<?php
new Callback(function($app, ...){ /* ... */ }, ['list parameters']);
new Callback('Function', ['list parameters']);
new Callback('Class::method', ['list parameters']);
new Callback(['Class', 'method'], ['list parameters']);
new Callback([$someObject, 'method'], ['list parameters']);
```

- $app->middleware($parameters)
- $response = function or method ($app, $parameters...)
- $app->terminate($response)

#### Controller

```php
new Controller('Class', 'method', ['list parameters']);
```

- $app->middleware($parameters)
- $Class = Class::__construct($app)
- $Class->middleware($parameters) // if exists
- $response = $Class->method(...)
- $Class->terminate() // if exists
- $app->terminate($response)

## License

MIT
