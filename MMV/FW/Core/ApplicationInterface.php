<?php

namespace MMV\FW\Core;

use MMV\FW\Core\RoutesInterface;
use Throwable;

interface ApplicationInterface
{
    public static function getRoutes(ApplicationInterface $app): RoutesInterface;

    /**
     * @param Throwable $exception
     * @param array $config
     * @param object|null $app
     * @return mixed
     */
    public static function phpErrorHandler(Throwable $exception, array $config, ?object $app=null): string;

    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     * 
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters);

    /**
     * @param mixed $response
     * @return mixed
     */
    public function terminate($response);
}
