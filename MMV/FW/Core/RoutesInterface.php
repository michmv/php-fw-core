<?php

namespace MMV\FW\Core;

use MMV\FW\Core\Execute;

interface RoutesInterface
{
    public function getExecute(): Execute;
}
