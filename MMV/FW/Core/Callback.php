<?php

namespace MMV\FW\Core;

use MMV\FW\Core\Execute;

class Callback extends Execute
{
    /**
     * @var callable
     */
    public $callback;

    /**
     * @param callable $callback
     * @param array $parameters
     */
    public function __construct($callback, array $parameters=[])
    {
        $this->callback = $callback;
        $this->parameters = $parameters;
    }
}
