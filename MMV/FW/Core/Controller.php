<?php

namespace MMV\FW\Core;

use MMV\FW\Core\Execute;

class Controller extends Execute
{
    public string $controller;

    public string $method;

    /**
     * @param string $controller
     * @param string $method
     * @param array $parameters
     */
    public function __construct(string $controller, string $method, array $parameters=[])
    {
        $this->controller = $controller;
        $this->method = $method;
        $this->parameters = $parameters;
    }
}
