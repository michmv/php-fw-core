<?php

namespace MMV\FW\Core;

use ErrorException;
use MMV\FW\Core\ApplicationInterface;
use Throwable;
use MMV\FW\Core\Controller;
use MMV\FW\Core\Callback;

trait ApplicationTrait
{
    public static function main(array &$config, bool $catchNoticeWarning=true): string
    {
        if($catchNoticeWarning) {
            set_error_handler(function(int $errno , string $errstr, string $errfile='', int $errline=0) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            });
        }

        try {
            $app = new static($config);

            return (string)static::execute($app); // result to string
        }
        catch (Throwable $e) {
            return static::phpErrorHandler($e, $config, $app ?? null);
        }
    }

    /**
     * This method is great for testing the query result.
     * 
     * @param \MMV\FW\Core\ApplicationInterface $app
     * @return mixed
     */
    public static function execute(ApplicationInterface $app)
    {
        $routes = static::getRoutes($app);
        $execute = $routes->getExecute();

        $response = $app->middleware($execute->parameters); // global middleware

        if(is_null($response)) {
            // middleware return null

            if($execute instanceof Controller) {
                // execute controller
                $controller = $execute->controller;
                $controller = new $controller($app);

                $response = null;
                
                if(method_exists($controller, 'middleware')) // local middleware for controller, if exists
                    $response = $controller->middleware($execute->parameters);
                
                if(is_null($response))
                    $response = call_user_func_array([$controller, $execute->method], $execute->parameters);
            }
            else if($execute instanceof Callback) {
                // execute callback function
                $response = call_user_func_array($execute->callback, array_merge([$app], $execute->parameters));
            }
            else {
                throw new \RuntimeException(sprintf('Can\'t execute aplication, unknown class %s', get_class($execute)));
            }

            // terminate for controller, if method exists
            if($execute instanceof Controller && method_exists($controller, 'terminate'))
                $response = $controller->terminate($response);
        }

        $response = $app->terminate($response); // global terminate

        return $response;
    }

    /**
     * If methos return not null to stop execute application and call
     * next terminate method
     * 
     * @param array $parameters
     * @return mixed|null
     */
    public function middleware(array $parameters)
    {
        // ...
        return null;
    }

    /**
     * @param mixed $response
     * @return mixed
     */
    public function terminate($response)
    {
        // ...
        return $response;
    }

    /**
     * @param Throwable $exception
     * @param array $config
     * @param object|null $app
     * @return mixed
     */
    public static function phpErrorHandler(Throwable $exception, array $config, ?object $app=null): string
    {
        if($config['debug']) {
            throw $exception;
        }
        else {
            error_log($exception); // write exception in log
            ob_end_clean();
            http_response_code(500);
            return '<h1>500 | Internal Server Error</h1>';
        }
    }
}
